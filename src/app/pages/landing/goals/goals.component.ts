import { Component, Input, OnInit } from '@angular/core';
import { Goals } from '../models/Ilanding';

@Component({
  selector: 'app-goals',
  templateUrl: './goals.component.html',
  styleUrls: ['./goals.component.scss']
})
export class GoalsComponent implements OnInit {
  @Input() public goals: Goals | undefined;

  constructor() { }

  ngOnInit(): void {
  }

}
