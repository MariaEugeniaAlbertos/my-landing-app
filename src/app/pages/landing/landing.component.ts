import { Intro, Goals, Newsletter } from './models/Ilanding';
import { Component, OnInit } from '@angular/core';
import { noUndefined } from '@angular/compiler/src/util';



@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {

  public intro: Intro;
  public goals: Goals;
  public newsletter: Newsletter = null;

  constructor() {
    this.intro = {
      imgIntro: {
        urlImg:'../../../assets/img/hipo.jpeg',
        altImg:'hippos image',
      },
      titleIntro:'Best African Safari Experiences',
      logo: {
        urlImg:'../../../assets/img/logo.png',
        altImg:'logo safari tours',
      },
      descriptionIntro: 'We are specialized in creating custom designed African adventures, tours and game safaris based entirely on your particular needs, personality and interests.'
    },
    this.goals = {
    imgGoals1: {
      urlImg:'../../../assets/img/elefante.png',
      altImg:'eleplhant icon',
    },
    titleGoals1: 'Customized',
    descriptionGoals1: 'Customized Safaris are designed for you, based on your dates, itinerary,budget and interests.  All you need to do is pack your bag and enjoy the special wonders of Africa.',
    imgGoals2: {
      urlImg:'../../../assets/img/lion.png',
      altImg:'lion icon',
    },
    titleGoals2: 'Unique',
    descriptionGoals2: 'Going on safari in Africa is one of the most exhilarating and memorable travel experiences. The thrill of spotting your first massive elephant or the calm of the savannah interrupted only by the curious sounds of nature... Every moment enlivens the senses and engrains itself deeply in your mind. ',
    imgGoals3: {
      urlImg:'../../../assets/img/jirafa.png',
      altImg:'giraffe icon',
    },
    titleGoals3: 'Budget',
    descriptionGoals3: 'Our safari packages are all-inclusive, that means that you pay a set daily rate per person that includes your accommodation, meals, snacks, standard drinks plus your game-viewing activities.',
    imgGoals4: {
      urlImg:'../../../assets/img/rino.png',
      altImg:'rhino image',
    },
    titleGoals4: 'Special Offers',
    descriptionGoals4: 'We constantly negotiate on your behalf with Africa’s top lodges and hotels to bring you vacation deals that make the most of your travel budget without compromising on the experience. Our promise is to always offer you the best seasonal rates available and real value for money.'

    }


  }

  ngOnInit(): void {}

  public subscribeNewsletter(newsletter: Newsletter) {
    
    this.newsletter = newsletter;
  }

}
