export interface Intro {
    imgIntro: Img;
    titleIntro: string;
    logo: Img;
    descriptionIntro: string;
}

export interface Img {
    urlImg: string;
    altImg: string;
}

export interface Goals {
    imgGoals1: Img;
    titleGoals1: string;
    descriptionGoals1: string;
    imgGoals2: Img;
    titleGoals2: string;
    descriptionGoals2: string;
    imgGoals3: Img;
    titleGoals3: string;
    descriptionGoals3: string;
    imgGoals4: Img;
    titleGoals4: string;
    descriptionGoals4: string;


}

export interface Newsletter {
    name: string;
    surname: string;
    mail: string;
    phone: number;
}
