import { Newsletter } from './../models/Ilanding';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { doesNotThrow } from 'assert';

@Component({
  selector: 'app-newsletter',
  templateUrl: './newsletter.component.html',
  styleUrls: ['./newsletter.component.scss'],
})
export class NewsletterComponent implements OnInit {
  public newsletterForm: FormGroup | null = null;
  public submitted: boolean = false;
  @Output() public newsletterEmmit = new EventEmitter<Newsletter>();

  constructor(private formBuilder: FormBuilder) {
    this.newsletterForm = this.formBuilder.group({
      name: [
        '',
        Validators.compose([Validators.required, Validators.minLength(1)]),
      ],
      surname: [
        '',
        Validators.compose([Validators.required, Validators.minLength(1)]),
      ],
      mail: ['', Validators.compose([Validators.required, Validators.email])],
      phone: [
        '',
        Validators.compose([Validators.required, Validators.minLength(9)]),
      ],
    });
  }

  ngOnInit(): void {}

  public onSubmit(): void {
    this.submitted = true;
    if (this.newsletterForm?.valid) {
      const newsletterRegister: Newsletter = {
        name: this.newsletterForm?.get('name')?.value,
        surname: this.newsletterForm?.get('surname')?.value,
        mail: this.newsletterForm?.get('mail')?.value,
        phone: this.newsletterForm?.get('phone')?.value,
      };

      this.sendNewsletter(newsletterRegister);

      console.log(newsletterRegister);
      this.newsletterForm?.reset();
      this.submitted = false;
    }
  }

  public sendNewsletter(newsletterRegister: Newsletter) {
    this.newsletterEmmit.emit(newsletterRegister);
  }
}
