import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingComponent } from './pages/landing/landing.component';
import { IntroComponent } from './pages/landing/intro/intro.component';
import { GoalsComponent } from './pages/landing/goals/goals.component';
import { NewsletterComponent } from './pages/landing/newsletter/newsletter.component';

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    IntroComponent,
    GoalsComponent,
    NewsletterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
